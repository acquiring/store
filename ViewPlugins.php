<?php
include "config/database.php";
include "config/sessionManager.php";
if(!LoggedIn()){
  header( "Location: Login.php" );
}
$currentUser=GetCurrnetUserData();
$userId=$currentUser['userid'];
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>acquiring Store</title>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/bootstrap-table.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">

<!--Icons-->
<script src="js/lumino.glyphs.js"></script>

<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>
<?php
include("blocks/nav.php");
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
  <div class="row">
    <ol class="breadcrumb">
      <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
      <li class="active">Icons</li>
    </ol>
  </div><!--/.row-->

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header"></h1>
    </div>
  </div><!--/.row-->

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">Plugins</div>
        <div class="panel-body">
          <table data-toggle="table" >
              <thead>
                <tr>
                    <th class="col-md-2">StoreID</th>
                    <th>Name</th>
                    <th class="col-md-2">Operation</th>
                </tr>
              </thead>

                <?php
                $conn=connectDB();
                $sql = "SELECT * FROM plugins WHERE userid='$userId'";
                $result = $conn->query($sql);
                if ($result->num_rows > 0){
                  while($row = $result->fetch_assoc()) {
                    $StoreID=$row['StoreID'];
                    $Name=$row['Name'];
                    echo "<tr><td>$StoreID</td><td>$Name</td><td><center>";
                    echo "<a type='Edit' class='btn btn-primary' href='Editplugin.php?id=$StoreID'>Edit</a><a> </a>";
                    echo "<a type='Delete' class='btn btn-primary' href='Deleteplugin.php?id=$StoreID'>Delete</a><center></td></tr>";
                  }
                }
                ?>
          </table>
        </div>
      </div>
    </div>



    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/chart.min.js"></script>
    <script src="js/chart-data.js"></script>
    <script src="js/easypiechart.js"></script>
    <script src="js/easypiechart-data.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/bootstrap-table.js"></script>
    <script>
      !function ($) {
        $(document).on("click","ul.nav li.parent > a > span.icon", function(){
          $(this).find('em:first').toggleClass("glyphicon-minus");
        });
        $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
      }(window.jQuery);

      $(window).on('resize', function () {
        if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
      })
      $(window).on('resize', function () {
        if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
      })
    </script>
    </body>

    </html>
