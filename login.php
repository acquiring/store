
<?php
include "config/database.php";
include "config/sessionManager.php";
if(LoggedIn()){
  header( "Location: index.php" );
}
$ErrorID=0;
if(isset($_POST["user_name"]) && isset($_POST["user_password"])) {
  $user_name=$_POST["user_name"];
  $password=md5($_POST["user_password"]);
  $ErrorID=setSession($user_name,$password);
  if($ErrorID==0){
    header( "Location: index.php" );
  }
}
?>


<?php
function GetErrors($ErrorID){
  if($ErrorID==1){
    echo "Username not exist";
  }
}
?>

<head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Login</title>
      <link rel="stylesheet" href="css/Users_Form_style.css">
      <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="css/normalize.css">
  </head>
  <body>

    <form action="login.php" method="post">

      <h1>Login</h1>

      <fieldset>
        <legend><span class="number">1</span>Your basic info</legend>
        <label for="name">Name: <a class="error"><?php echo GetErrors($ErrorID); ?></a> </label>
        <input type="text" id="name" name="user_name" value="<?php echo isset($_POST['user_name']) ? $_POST['user_name'] : '' ?>">

        <label for="password">Password:</label>
        <input type="password" id="password" name="user_password" value="<?php echo isset($_POST['user_password']) ? $_POST['user_password'] : '' ?>">

      </fieldset>
      <button type="submit">Login</button>
      <p></p>
      <fieldset>
        <legend><span class="number">2</span>Create New account</legend>
        <a href="register.php"><button type="button">Register</button></a>
      </fieldset>

    </form>

  </body>
</html>
