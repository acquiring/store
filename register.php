
<?php
include "config/database.php";
include "config/sessionManager.php";
if(LoggedIn()){
  header( "Location: index.php" );
}
$ErrorID=0;

if(isset($_POST["user_name"]) && isset($_POST["user_email"]) && isset($_POST["user_password"])) {
  $user_name=$_POST["user_name"];
  $user_email=$_POST["user_email"];
  $password=md5($_POST["user_password"]);
  $conn=connectDB();
  $sql = "SELECT * FROM users WHERE name='$user_name'";
  $result = $conn->query($sql);
  if ($result->num_rows > 0){
    $ErrorID=1;
  }else{
    $sql = "SELECT * FROM users WHERE email='$user_email'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
      $ErrorID=2;
    }else{
      $sql = "INSERT INTO users (name, email, password) VALUES ('$user_name', '$user_email', '$password')";
      if ($conn->query($sql) === TRUE) {
          echo "New record created successfully";
          header( "Location: login.php" );
      } else {
          echo "Error: " . $sql . "<br>" . $conn->error;
      }
    }
  }

}
?>


<?php
function GetErrors($Case,$ErrorID){
  if($Case=="UserName"){
    if($ErrorID==1){
      echo "Username already exist";
    }
  }
  else if($Case=="Email"){
    if($ErrorID==2){
      echo "email already exist";
    }
  }
}
?>

<head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Sign Up Form</title>
      <link rel="stylesheet" href="css/Users_Form_style.css">
      <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="css/normalize.css">
  </head>
  <body>

    <form action="register.php" method="post">

      <h1>Sign Up</h1>

      <fieldset>
        <legend><span class="number">1</span>Your basic info</legend>
        <label for="name">Name: <a class="error"><?php echo GetErrors("UserName",$ErrorID); ?></a> </label>
        <input type="text" id="name" name="user_name" value="<?php echo isset($_POST['user_name']) ? $_POST['user_name'] : '' ?>">

        <label for="mail">Email: <a class="error"><?php echo GetErrors("Email",$ErrorID); ?></a> </label>
        <input type="email" id="mail" name="user_email" value="<?php echo isset($_POST['user_email']) ? $_POST['user_email'] : '' ?>">

        <label for="password">Password:</label>
        <input type="password" id="password" name="user_password" value="<?php echo isset($_POST['user_password']) ? $_POST['user_password'] : '' ?>">

      </fieldset>
      <button type="submit">Sign Up</button>
      <p></p>
      <fieldset>
        <legend><span class="number">2</span>Login</legend>
        <a href="login.php"><button type="button">Login</button></a>
      </fieldset>


    </form>



  </body>
</html>
