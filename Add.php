<?php
include "config/database.php";
include "config/sessionManager.php";
if(!LoggedIn()){
  header( "Location: Login.php" );
}
?>
<?php
include("blocks/header.php");
?>
<body>
<?php
include("blocks/nav.php");
?>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			</ol>
		</div><!--/.row-->

    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">add New plugin</div>
          <div class="panel-body">
            <div class="col-md-6">
    <form role="form" action="addPlugin.php" method="post" autocomplete="on" enctype= "multipart/form-data">

      <div class="form-group">
        <label>Plugin Name</label>
        <input name="Name" class="form-control" placeholder="Placeholder">
      </div>

      <div class="form-group">
        <label>Plugin Description</label>
        <input name="Description" class="form-control" placeholder="Placeholder">
      </div>

      <div class="form-group">
        <input type="file" name="fileToUpload" id="fileToUpload">
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
</div>
</div><!-- /.col-->
</div><!-- /.row -->
	</div>	<!--/.main-->
<?php include("blocks/footer.php"); ?>
