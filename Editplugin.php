<?php
include "config/database.php";
include "config/sessionManager.php";
if(!LoggedIn()){
  header( "Location: Login.php" );
}
$currentUser=GetCurrnetUserData();
$userId=$currentUser['userid'];
$StoreID=$_REQUEST['id'];
echo $StoreID;
$conn=connectDB();
if(!ApprovedOperationOnPlugin($StoreID,$userId)){
  header( "Location: ViewPlugins.php" );
}
if(isset($_POST["Name"]) && isset($_POST["Description"])){
  $Name=$_POST["Name"];
  $Description=$_POST["Description"];
  $sql = "UPDATE plugins SET Name='$Name',Description='$Description' WHERE StoreID=$StoreID";
  $conn->query($sql);
  header( "Location: ViewPlugins.php" );
}

$sql = "SELECT * FROM plugins WHERE StoreID='$StoreID'";
$result = $conn->query($sql);
$plugin = $result->fetch_assoc();
?>
<?php
include("blocks/header.php");
?>
<body>
<?php
include("blocks/nav.php");
?>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
			</ol>
		</div><!--/.row-->

    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">Edit plugin</div>
          <div class="panel-body">
            <div class="col-md-6">
    <form role="form" action="" method="post" autocomplete="on" enctype= "multipart/form-data">

      <div class="form-group">
        <label>Plugin Name</label>
        <input name="Name" class="form-control" placeholder="Placeholder" value="<?php echo $plugin['Name'];?>">
      </div>

      <div class="form-group">
        <label>Plugin Description</label>
        <input name="Description" class="form-control" placeholder="Placeholder" value="<?php echo $plugin['Description'];?>">
      </div>
      <button type="submit" class="btn btn-primary">Upadte</button>
    </form>
  </div>
</div>
</div><!-- /.col-->
</div><!-- /.row -->
	</div>	<!--/.main-->
<?php include("blocks/footer.php"); ?>
